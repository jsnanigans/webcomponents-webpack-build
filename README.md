# Component library

#### Components
- [Button](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/button/)
- [Switch](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/switch)
- [Cookie header](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/cookie)
- [Overlay](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/overlay)
- [Tabs](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/tabs)
- [Input](https://bitbucket.org/loop-dev/loop-components/src/master/src/components/input)

#### Internal libraries
- [Scroll handling](https://bitbucket.org/loop-dev/loop-components/src/master/src/lib/scrolling)
- [Store](https://bitbucket.org/loop-dev/loop-components/src/master/src/lib/store)

# Implementation
## Lazy
```html
<script nomodule src="https://components.view.agentur-loop.com/v1/es5/babelPolyfill.js"></script>
<script nomodule src="https://components.view.agentur-loop.com/v1/es5/wcPolyfill.js"></script>
<script nomodule src="https://components.view.agentur-loop.com/v1/es5/lazy.js"></script>
<script type="module" src="https://components.view.agentur-loop.com/v1/wcPolyfill.js"></script>
<script type="module" src="https://components.view.agentur-loop.com/v1/lazy.js"></script>
```

Put this script into the `head` to avoid some of the flashing of content.

Browsers that support JS Modules will ignore script tags with `nomodule` and browser that dont will ignore script tags with `type="module"`

To avoid flashing of certain elements completely you can check the name of the component in the network tab of your dev-tools of choice, for example `loop-button.js` and add it below the `lazy.js` script tag like so:
```html
<script nomodule src="https://components.view.agentur-loop.com/v1/lib/loop-button.js"></script>
<script type="module" src="https://components.view.agentur-loop.com/v1/es5/lib/loop-button.js"></script>
```
dont forget to add the es5 version for older browsers.

### Element detection
In the `lazy` script a method `window.detectModules` is defined on the window scope which is called immediately and on dom ready. A [MutationObserver](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver) is added on the `body` node to detect any new elements added asynchronous, when it detects a change then `window.detectModules` is called again.

# Theming
Using Custom Properties you can define the theme. Here is how you would use them, and these are the default values. You can define them on any element, it does not have to be `:root`
```css
:root {
  --loop-theme--primary: #607d8b;
  --loop-theme--on-primary: #fff;

  --loop-theme--secondary: #29b6f6;
  --loop-theme--on-secondary: #fff;

  --loop-theme--tertiary: #29b6f6;
  --loop-theme--on-tertiary: #fff;

  --loop-theme--surface: #fff;
  --loop-theme--on-surface: #000;

  --loop-theme--background: #fff;
  --loop-theme--error: #b00020;
  --loop-theme--hint: #ae5204;
  --loop-theme--disabled: #ccc;
  --loop-theme--icon: #000;

  --loop-theme--transition-duration: .2s;
  --loop-theme--transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
}
```