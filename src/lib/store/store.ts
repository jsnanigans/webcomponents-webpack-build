export default class Molly {
  public get actions() {
    return this.action
  }
  public store: object
  public action: any = {}
  public listeners: any[] = []

  constructor(storeObject: object) {
    this.store = storeObject
  }

  public addAction(key: string, method: (value: any) => any) {
    const value = this.getValue(key)
    this.action[key] = method
  }

  // private watch (key: string, method: (storeValue) => void) {
  //   this.addListener(key, method)
  // }

  public watch(
    path: string,
    method: (storeValue) => void,
    options: MollyWatchOptions = {},
  ) {
    if (!path) {
      return
    }

    if (path.indexOf('.') === -1) {
      this.addListener(path, method, options)
      return
    }

    // deep
    const parts = path.split('.')
    const root = this.removeModifiers(parts[0])

    this.addListener(
      root,
      (branch) => {
        method(this.getValue(path))
      },
      options,
    )
  }

  public getValue(path: string) {
    const parts = path.split('.')

    const modifiers = this.parseModifiers(parts[0])
    parts[0] = this.removeModifiers(parts[0])

    const root = parts[0]

    let value: any = this.store
    for (const key of parts) {
      if (value instanceof Object) {
        value = value[key]
      } else {
        break
      }
    }

    value = this.applyModifiers(modifiers, value)

    return value
  }

  public set(path: string, mutation: (storeValue) => any) {
    if (!path) {
      return
    }

    if (typeof mutation !== 'function') {
      console.error('mutations must be a method')
    }

    const modifiers = this.parseModifiers(path)
    const value = this.getValue(path)

    const newValue = mutation(this.applyModifiers(modifiers, value))
    const initialPath = path

    const cleanPath = this.removeModifiers(path)

    if (cleanPath.indexOf('.') === -1) {
      // root change
      if (newValue !== value) {
        this.store[cleanPath] = newValue
      }
    } else {
      // deep
      const parts = cleanPath.split('.')
      const root = parts[0]
      path = root
      const target = parts[parts.length - 1]
      const rootClone = { ...this.store[root] }
      let tmpValue = rootClone

      for (const key of parts) {
        if (key !== root) {
          if (tmpValue instanceof Object) {
            if (target === key) {
              tmpValue[key] = newValue
            } else {
              tmpValue = tmpValue[key]
            }
          }
        }
      }

      this.store[root] = rootClone
    }

    // notify listeners
    const l = this.listeners.length

    for (let i = 0; i < l; i++) {
      const key = this.listeners[i].key
      if (key === cleanPath || cleanPath.indexOf(key) === 0) {
        const options = this.listeners[i].options || {}
        const sendValue = options.root
          ? this.applyModifiers(
              this.listeners[i].modifiers,
              this.getValue(this.listeners[i].key),
            )
          : this.applyModifiers(this.listeners[i].modifiers, newValue)
        this.listeners[i].method(sendValue)
      }
    }
  }

  private addListener(
    key: string,
    method: (val: any) => any,
    options: MollyWatchOptions,
  ) {
    const modifiers = this.parseModifiers(key)
    const cleanKey = this.removeModifiers(key)

    this.listeners.push({
      key: cleanKey,
      method,
      options,
      modifiers,
    })

    const value = this.getValue(key)
    if (typeof value !== 'undefined' && options.eager !== false) {
      method(this.getValue(key))
    }
  }

  private applyModifiers(modifiers: any, value: any) {
    if (modifiers.eq) {
      const eq = modifiers.eq[0]
      const compare = modifiers.eq[1]

      switch (eq) {
        case '<':
          value = compare < parseFloat(value)
          break
        case '>':
          value = compare > parseFloat(value)
          break
        case '>=':
          value = compare >= parseFloat(value)
          break
        case '<=':
          value = compare <= parseFloat(value)
          break
      }

    }
    if (modifiers.invert) {
      value = !value
    }
    return value
  }

  private parseModifiers(str) {
    const rt: any = {}
    let useStr = str
    const parts = str.split(' ')

    if (parts.length === 1) {
      useStr = parts[parts.length - 1]
    } else {
      const eq = parts[1]
      const compare = parseFloat(parts[0])
      rt.eq = [eq, compare]
    }

    if (useStr.indexOf('!') === 0) {
      rt.invert = true
    }

    return rt
  }
  private removeModifiers(str: string) {
    const parts = str.split(' ')
    const use = parts[parts.length - 1]
    return use.replace(/^(!)/g, '')
  }
}
// document.addEventListener('DOMContentLoaded', () => {

//   const el1: HTMLSpanElement = document.querySelector('#count')
//   const el2: HTMLSpanElement = document.querySelector('#count2')

//   Molly.watch('counter', (e) => {
//     el1.innerText = e
//   })
//   Molly.watch('counter', (e) => {
//     el2.innerText = e
//   })

//   setInterval(() => {
//     Molly.set('counter', (e) => (e + 1))
//   }, 20)
// })
