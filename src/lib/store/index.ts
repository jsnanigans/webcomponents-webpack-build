import addStuff from './generic'
import Molly from './store'

const store = new Molly({})
addStuff(store)

if (typeof window !== 'undefined') {
  window.loops = store
}

export default store
