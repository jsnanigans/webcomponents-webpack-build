# Molly the store manager

Lets you create listeners from anywhere and notifies you when a value in the store has changed.

![Molly](https://i.imgur.com/DcBWBPx.jpg)

## Usage
Initiate your store with some data or just a empty object
```javascript
const store = new Molly({})
```

> The store is registered on the window scope with the name `loops`, short for "loop store"

## Watch for changes
You can watch for changes in the store with the `watch` method. The first parameter must be a `string` which is the key in the store, the second is a `method` that runs when the data changes and when the watcher is setup initially.
```javascript
store.watch('user_auth_token', (token) => {
  // runs whenever `user_auth_token` the data changes
})
```

you can also watch for "deeper" changes inside objects inside the store by passing a string with a `.` separator like so:
```javascript
store.watch('user.token', (token) => {
  // runs whenever `user.token` changes
})
```

### watch options

Here are the options with the default value:
```javascript
store.watch('user.token', (token) => {}, {
  eager: true, // run method as soon as watcher is set with current store value
  root: false, // return the whole root key in the store e.g. the full user object
})
```

## Set value
To set a value in the store use the `set` method. The first argument must be a `string` that is the key for the store, the second must be a `method`, it gets passed one parameter that is the current value of the store, whatever is returned becomes the new value

```javascript
store.set('user_auth_token', () => '<<mytoken>>')
```
to set a value deeper in the store use this syntax:
```javascript
store.set('user.token', () => '<<mytoken>>')
```


## Actions
Define global functions from inside some scope that can be called from anywhere

Add a new action to the store
```javascript
store.addAction('notify_user', (data) => {
  this.removeOldNotifications()
  this.showNotification(data)
})
```
then you can call it from anywhere else
```html
<loop-button onclick="store.action.notify_user('Hello user')">Say hello</loop-button>
```
