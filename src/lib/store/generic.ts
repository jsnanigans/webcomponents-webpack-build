export default function addStuff(store: MollyStore) {
  store.set('windowWidth', () => window.innerWidth)
  store.set('windowHeight', () => window.innerHeight)

  let to: number
  window.addEventListener('resize', () => {
    clearTimeout(to)
    to = setTimeout(() => {
      store.set('windowWidth', () => window.innerWidth)
      store.set('windowHeight', () => window.innerHeight)
    }, 16)
  })
}

export function computeAttributes(
  store: MollyStore,
  name: string = 'loops',
  target: HTMLElement | Document = document) {
  const elements = target.querySelectorAll(`[${name}-model]`) as NodeListOf<StoreModelElement>

  for (const el of elements) {
    if (!el.storeInitiated) {
      el.storeInitiated = true

      const model = el.getAttribute(`${name}-model`)
      const conditionalClass = el.getAttribute(`${name}-class-if-model`)

      store.watch(model, (value: boolean) => {
        if (conditionalClass) {
          el.classList.toggle(conditionalClass, !!value)
        }

        if (typeof el.onchange === 'function') {
          el.onchange()
        }
      })
    }
  }
}
