import store from '../store/index'

store.addAction('registerValidator', (data: LoopRegisterValidator) => {
  const customValidators = store.getValue('customValidators') || {}
  customValidators[data.key] = data.handler
  store.set('customValidators', () => ({...customValidators}))
})

const parseNumber = (str: string): number => {
  let parsed: number = parseInt(str, 10)

  if (isNaN(parsed)) {
    parsed = parseInt(str.replace(/\D/, ''), 10)
  }

  return parsed
}

const preset: LoopValidators = {
  '*': (e) => ({valid: true}),
  'req': (e) => ({
    message: `${e.name} is required`,
    valid: !!e.value,
  }),
  'len': (e) => {
    const len = parseNumber(e.meta)

    return {
      valid: e.value.length === len,
      message: `${e.name} must be ${len} characters long`,
    }
  },
  'minlen': (e) => {
    const len = parseNumber(e.meta)

    return {
      valid: e.value.length >= len,
      message: `${e.name} must be at least ${len} characters long`,
    }
  },
  'maxlen': (e) => {
    const len = parseNumber(e.meta)

    return {
      valid: e.value.length <= len,
      message: `${e.name} can't be more than ${len} characters long`,
    }
  },
}

export default preset
