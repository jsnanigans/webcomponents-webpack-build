import '@polymer/polymer/polymer-element'
import './lib/scrolling/scrolling'
import { computeAttributes } from './lib/store/generic'
import store from './lib/store/index'

// used libraries
import anime from 'animejs'
window.anime = anime

// reset style
import './reset.inline.scss'

// Core modules
window.loadModule = {
  'loop-button': () => import(/* webpackChunkName: "loop-core" */ './components/button/loop-button'),
  'loop-if': () => import(/* webpackChunkName: "loop-core" */ './components/logic/loop-if'),
  'loop-nav': () => import(/* webpackChunkName: "loop-core" */ './components/navigation/loop-nav'),
}

window.loadModule = {
  ...window.loadModule,

  // Separate modules
  'loop-dropdown': () => import(/* webpackChunkName: "loop-cookie" */ './components/dropdown/loop-dropdown'),
  'loop-cookie': () => import(/* webpackChunkName: "loop-cookie" */ './components/cookie/loop-cookie-header'),
  'loop-number-field': () => import(/* webpackChunkName: "loop-field" */ './components/input/index'),
  'loop-overlay': () => import(/* webpackChunkName: "loop-overlay" */ './components/overlay/loop-overlay'),
  'loop-sidebar': () => import(/* webpackChunkName: "loop-sidebar" */ './components/sidebar/loop-sidebar'),
  'loop-switch': () => import(/* webpackChunkName: "loop-switch" */ './components/switch/loop-switch'),
  'loop-tabs': () => import(/* webpackChunkName: "loop-tabs" */ './components/tabs/loop-tabs'),
  'loop-text-field': () => import(/* webpackChunkName: "loop-field" */ './components/input/index'),
}

window.detectModules = (root = document) => {
  computeAttributes(store)
  for (const key in window.loadModule) {
    if (key) {
      const query = root.querySelector(key)
      if (query) {
        window.loadModule[key]()
      }
    }
  }
}

window.detectModules()
document.addEventListener('DOMContentLoaded', (event) => {
  window.detectModules()
  observer.observe(document.body, config)
})

// mutation observer
const config = { attributes: false, childList: true, subtree: true }
const observer = new MutationObserver((mutationsList) => {
  for (const mutation of mutationsList) {
    if (mutation.type === 'childList') {
      window.detectModules()
    }
  }
})
