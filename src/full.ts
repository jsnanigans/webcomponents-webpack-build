import '@polymer/polymer/polymer-element'
import './lib/scrolling/scrolling'
import './lib/store/index'

import './components/button/loop-button'
import './components/cookie/loop-cookie-header'
import './components/overlay/loop-overlay'
import './components/switch/loop-switch'
import './components/tabs/loop-tabs'
