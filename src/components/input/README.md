# Inputs

# Text
## Html
```html
<loop-text-field label="Some Label Here"></loop-text-field>
<loop-text-field label="A Label" value="and a value"></loop-text-field>
```
## Slots
- none

## Attributes / Config
- `label` - Adds a label that moves out of the way when focused or has value
- `placeholder` - Adds a placeholder, is hidden if label is present
- `rounded` - Rounds corners
- `disabled` - Disables input

## CSS Custom properties exposed:
```css
:host {
  margin: prop(input-margin, 0.1em 0);
  font-size: prop(input-font-size, 1em);
}
:host([rounded]) {
  border-radius: prop(input-height, $height);
}
.lc_input {
  height: prop(input-height, $height);
  border: prop(input-border, 0.11em solid #fff);
  background: prop(input-background, #fff);
}
.lc_input__label {
  left: prop(input-padding-left, 1.6em);
  color: prop(input-label-color, #333);
}
.lc_input__placeholder {
  color: prop(input-placeholder-color, #333);
  left: prop(input-padding-left, 1.6em);
}
.lc_input input {
  min-height: prop(input-height, $height);
  padding-left: prop(input-padding-left, 1.6em);
  padding-right: prop(input-padding-right, 1.6em);
}
:host([state-focused]) .lc_input {
  border-color: prop(input-focus-border, #333);
}
:host([state-value][label]) .lc_input__label,
:host([state-focused][label]) .lc_input__label {
  color: prop(primary);
  color: prop(input-active-label-color);
}
```

# Number
```html
<loop-number-field rounded value="55" label="Number input"></loop-number-field>
<loop-number-field name="Amount" label="0 - 10" min-value="0" max-value="10" controls></loop-number-field>
<!-- Controls: -->
<loop-number-field name="Books Count" rounded controls value="3" style="width: 140px; text-align: center;"></loop-number-field>
```
## Slots
- `start` content before input
- `end` content after input

## Attributes / Config
- `label` - Adds a label that moves out of the way when focused or has value
- `placeholder` - Adds a placeholder, is hidden if label is present
- `rounded` - Rounds corners
- `disabled` - Disables input
- `controls` - Enable controls
- `min-value` - Minimum value
- `max-value` - Maximum value
- `extra-start` - Shows the content in the slot `start`
- `extra-end` - Shows the content in the slot `end`
- `value` - Default value

## CSS Custom properties exposed:
```css
// all from text input and additionally:

:host([rounded]) {
  .lc_input__control--up {
    border-radius: prop(input-control-radius-right, 0 $height $height 0);
  }
  .lc_input__control--up {
    border-radius: prop(input-control-radius-left, $height 0 0 $height);
  }
}

.lc_input__control {
  padding: prop(input-control-padding, 0 1.2em);
}
.lc_input__control:focus::before {
  background: prop(input-control-active-bg, rgba(0,0,0,.02));
}
.lc_input__label {
  left: prop(input-control-padding-left, .2em);
}
.lc_input input {
  padding-left: prop(input-control-padding-left, .2em);
  padding-right: prop(input-control-padding-right, .2em);
}

.lc_input__extra {
  font-size: prop(input-extra-font-size, 1.25em);
}
.lc_input__extra--end {
  padding: prop(input-extra-padding-end, 0 1.1em 0 0);
}
.lc_input__extra--start {
  padding: prop(input-extra-padding-start, 0 1.1em 0 0);
}
```