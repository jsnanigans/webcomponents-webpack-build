import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'
import baseValidators from '../../lib/validator/index'
import baseStyles from './input.scss'
const loadAsyncValidator = () => import(/* webpackChunkName: "validator" */ '../../lib/validator/async')
// import styles from './input-text.scss'

export default class LoopTextField extends PolymerElement {

  static get properties() {
    return {
      value: {
        type: String,
        value: '',
        notify: true,
        observer: '_valueChanged',
      },
      placeholder: {
        type: String,
        value: '',
        reflectToAttribute: true,
      },
      error: {
        type: String,
        reflectToAttribute: true,
      },
      label: {
        type: String,
        reflectToAttribute: true,
      },
      name: {
        type: String,
        value: '',
        reflectToAttribute: true,
      },
      required: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      validate: {
        type: String,
        value: '*',
      },
      disabled: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_disabledChanged',
      },
      rounded: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    }
  }

  static get template() {
    return html`
      <style>
        ${html([baseStyles])}
      </style>
      <div class="lc_input">
        <div class="lc_input__extra lc_input__extra--start"><slot name="start"></slot></div>
        <div class="lc_input__inner">
          <label class="lc_input__label">
            <span class="lc_input__label-text">[[label]]</span><span class="lc_input__error">[[error]]</span>
          </label>
          <input
            on-input="handleInput"
            on-change="handleChange"
            on-focus="handleFocus"
            on-blur="handleBlur"
          type="text" />
          <span class="lc_input__placeholder">[[placeholder]]</span>
        </div>
        <div class="lc_input__extra lc_input__extra--end"><slot name="end"></slot></div>
      </div>
    `
  }
  public state = 'null'
  public value: string
  public inputEl: HTMLInputElement
  public disabled: boolean
  public label: string
  public error: string
  public placeholder: string
  public name: string
  public validators: LoopValidators = baseValidators
  public validate: string
  public required: boolean

  public shadowRoot: HTMLElement
  public dispatchEvent: (e: Event) => void
  public setAttribute: (attr: string, value?: any) => void
  public removeAttribute: (attr: string) => void

  public validateTimeout

  public connectedCallback() {
    super.connectedCallback()
    this.inputEl = this.shadowRoot.querySelector('input')
    this.inputEl.value = this.value
    this.setState(this.value ? 'value' : 'initial')

    afterNextRender(this, async () => {
      this.inputEl.setAttribute('aria-label', this.label || this.placeholder || this.name)
      if (this.disabled) {
        this.inputEl.disabled = true
      }

      const validators = await loadAsyncValidator()
      this.validators = { ...this.validators, ...validators.default }
    })
  }

  public focus() {
    this.inputEl.focus()
    return this.inputEl
  }
  public blur() {
    this.inputEl.blur()
    return this.inputEl
  }
  public select() {
    this.inputEl.select()
    return this.inputEl
  }
  public handleInput() {
    this.value = this.inputEl.value

    let to = 500
    if (this.error) {
      to = 0
    }
    clearTimeout(this.validateTimeout)
    this.validateTimeout = setTimeout(() => {
      this.validateInput()
    }, to)
  }
  public handleChange() {
    this.value = this.inputEl.value

    clearTimeout(this.validateTimeout)
    this.validateTimeout = setTimeout(() => {
      this.validateInput()
    }, 10)
  }
  public handleFocus() {
    this.setState('focused')
  }
  public handleBlur() {
    this.setState(this.value ? 'value' : 'initial')
    clearTimeout(this.validateTimeout)
    this.validateTimeout = setTimeout(() => {
      this.validateInput()
    }, 10)
  }

  public validateInput() {
    const customValidators = store.getValue('customValidators') || {}
    const allValidators = {...customValidators, ...this.validators}
    const validateFor = this.validate.split(',')
    const isRequired = this.required || validateFor.indexOf('req') !== -1
    if (this.required && validateFor.indexOf('req') === -1) {
      validateFor.push('req')
    }

    if (!isRequired && typeof this.value === 'string' && this.value.length === 0) {
      this.error = undefined
      return
    }

    const params: LoopValidatorParams = {
      name: this.label || this.placeholder || this.name,
      value: this.value,
    }

    const checks = []
    for (const check of validateFor) {
      const [key, meta] = check.split(':')
      if (allValidators[key]) {
        checks.push(allValidators[key]({...params, meta}))
      } else {
        console.warn(`No validator with they key "${key}" is registered`)
      }
    }

    let errorMessage
    for (const check of checks) {
      if (check.valid === false) {
        errorMessage = check.message || `${params.name} is invalid`
      }
    }

    this.error = errorMessage
  }

  public setState(state: string) {
    const oldState = this.state
    this.state = state

    if (oldState) {
      this.removeAttribute(`state-${oldState}`)
    }

    this.setAttribute(`state-${state}`, '')
  }

  public _valueChanged(to, from) {
    if (this.inputEl && to !== from) {
      this.inputEl.value = to
    }
  }

  public _disabledChanged(to, from) {
    if (this.inputEl) {
      this.inputEl.disabled = to
    }
  }
}

window.customElements.define('loop-text-field', LoopTextField)
