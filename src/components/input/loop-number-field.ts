import { html } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import LoopTextField from './loop-text-field'

import controlStyles from './input-controls.scss'
import baseStyles from './input.scss'

export default class LoopNumberField extends LoopTextField {

  static get properties () {
    return {
      ...super.properties,
      controls: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      step: {
        type: Number,
        value: 1
      },
      decimals: {
        type: Number,
        value: 2
      },
      minValue: {
        type: Number,
        value: -Infinity
      },
      maxValue: {
        type: Number,
        value: Infinity
      },
    }
  }

  static get template () {
    return html`
      <style>
        ${html([baseStyles])}
        ${html([controlStyles])}
      </style>
      <div class="lc_input">
        <div class="lc_input__control lc_input__control--down"
          on-click="decreaseHandler"
          on-keypress="decreaseHandler"
        ><slot name="down">&lsaquo;</slot></div>

        <div class="lc_input__inner">
          <label class="lc_input__label">[[label]]</label>
          <input
            on-input="handleInput"
            on-change="handleChange"
            on-focus="handleFocus"
            on-blur="handleBlur"
            on-keydown="handleKeyDown"
          type="text" />
          <span class="lc_input__placeholder">[[placeholder]]</span>
        </div>

        <div class="lc_input__control lc_input__control--up"
          on-click="increaseHandler"
          on-keypress="increaseHandler"
        ><slot name="up">&rsaquo;</slot></div>
      </div>
    `
  }
  public controls: boolean
  public step: number
  public decimals: number
  public minValue: number
  public maxValue: number
  public controlEl: any = []

  public _valueChanged (to, from = '') {
    this.setValue(to, from)
  }

  public setValue (to, from = '') {
    if (to === '' || to === '-') {
      this.value = to
      return
    }
    let parsedValue = to

    if (typeof parsedValue !== 'number') {
      parsedValue = this.parsedValue(to)
    }

    // min-max
    if (parsedValue > this.maxValue) {
      parsedValue = this.maxValue
    }
    if (parsedValue < this.minValue) {
      parsedValue = this.minValue
    }

    // set decimals
    if (Math.round(parsedValue) === parsedValue) {
      parsedValue = (parsedValue).toFixed(0)
    } else {
      parsedValue = (parsedValue).toFixed(this.decimals)
    }

    const split = parsedValue.split('')
    let set : any = []

    for (const l of split) {
      if (/(\d|\.|-)/.test(l)) {
        set.push(l)
      }
    }

    if (this.inputEl) {
      set = set.join('')
      this.inputEl.value = set
      this.value = set
      this.setState('value')
    }
  }

  public connectedCallback () {
    super.connectedCallback()
    if (this.value) {
      this._valueChanged(this.value)
    }

    afterNextRender(this, () => {
      if (this.controls) {
        this.controlEl = this.shadowRoot.querySelectorAll('.lc_input__control')

        for (const el of this.controlEl) {
          el.setAttribute('tabindex', this.disabled ? '-1' : '0')
        }
        this.controlEl[0].setAttribute('aria-label', `Decrease ${this.name || this.label || 'Value'}`)
        this.controlEl[1].setAttribute('aria-label', `Increase ${this.name || this.label || 'Value'}`)
      }
    })
  }

  public handleInput () {
    super.handleInput()
    this.keydownEvent()
  }
  public handleChange () {
    super.handleChange()
    this.keydownEvent()
  }
  public handleKeyDown () {
    this.keydownEvent()
  }

  public increaseHandler (e) {
    // increase value only if click (1) or enter (13)
    if (e.which === 1 || e.which === 13) {
      const value = this.parsedValue() + this.step
      this.setValue(value)
    }
  }

  public decreaseHandler (e) {
    // decrease value only if click (1) or enter (13)
    if (e.which === 1 || e.which === 13) {
      const value = this.parsedValue() - this.step
      this.setValue(value)
    }
  }

  public parsedValue (val= this.value) {
    const parsed = parseFloat(this.value)
    return isNaN(parsed) ? 0 : parsed
  }

  public _disabledChanged (to, from) {
    super._disabledChanged(to, from)
    for (const el of this.controlEl) {
      el.setAttribute('tabindex', to ? '-1' : '0')
    }
  }

  private keydownEvent () {
    this._valueChanged(this.inputEl.value)
  }
}

window.customElements.define('loop-number-field', LoopNumberField)
