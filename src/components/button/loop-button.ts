import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import store from '../../lib/store/index'

import template from './button.html'
import styles from './button.scss'

export default class LoopButton extends PolymerElement {
  static get properties() {
    return {
      mounted: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      rounded: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      secondary: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      tertiary: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      model: {
        type: String,
        value: '',
      },
      disabled: {
        type: Boolean,
        reflectToAttribute: true,
      },
      round: {
        type: Boolean,
        reflectToAttribute: true,
      },
      large: {
        type: Boolean,
        reflectToAttribute: true,
      },
      ghost: {
        type: Boolean,
        reflectToAttribute: true,
      },
      flex: {
        type: Boolean,
        reflectToAttribute: true,
      },
      badge: {
        type: String,
        value: false,
        reflectToAttribute: true,
      },
    }
  }
  public mounted: boolean
  public model: string
  public disabled: any

  public setAttribute: (attr: string, value: any) => void
  public tabIndex: number
  public addEventListener: any
  public click: any

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.mounted = true
      this.tabIndex = 0
      store.watch(this.model, (data) => {
        this.disabled = data
      })
      this.addListeners()
    })
  }

  private addListeners() {
    this.addEventListener('keypress', (e: KeyboardEvent) => {
      if (e.which === 13) {
        this.click()
      }
    })
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
}

window.customElements.define('loop-button', LoopButton)
