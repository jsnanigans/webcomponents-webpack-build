# Button

## Html
```html
<loop-button>With Negation modifier</loop-button>
<loop-button large round secondary badge="2">🚀</loop-button>
```

## Slots
- `default` HTML inside the button

## Attributes / Config
- `model` Binds the `disabled` attribute to the value in the store, but converts it to boolean. It will update the attribute when the store value changes.

  Modifiers are added to the beginning of the key for the store, they mutate the value in some way. Modifiers are added to the beginning of the store key
  - `!` will negate the value - true will became false and false will become true

- `secondary` - use secondary colors
- `tertiary` - use tertiary colors
- `large` makes everything a bit bigger
- `badge` - adds a small badge on the top right of the button with the content passed to the attribute
- `rounded` rounds the corners
- `round` makes the button into a circle, this is for icon buttons
- `invert` stat with hover color (#fff by default) and use primary or seconday on hover, also removes border
- `ghost` makes the button hidden and only shows the content
- `flex` make the button grow in height and align content in center
- `center` align text in center

## CSS Custom properties exposed:
```css
:host {
  /* you can set the border but the border color will be overwritten by the seconday or primary color */
  border: prop(button-border, 0.11em solid #000);
  height: prop(button-height, 2.8em);
  padding: prop(button-padding, 0.09em 1.6em 0 1.6em);
  margin: prop(button-margin, 0.1em 0);
}
.badge {
  transform: prop(button-badge-transform, translate(12%, -50%));
  background: prop(button-badge-background, #333);
  color: prop(button-badge-color, #fff);
  font-size: prop(button-badge-font-size, 1rem);
  min-width: prop(button-badge-size, $size);
  height: prop(button-badge-size, $size);
  line-height: prop(button-badge-size, $size);
  border-radius: prop(button-badge-size, $size);
}
:host(:not([ghost]):hover) {
  background: prop(button-primary-hover-background, prop(on-primary));
  border-color: prop(button-primary-hover-border, prop(on-primary));
  color: prop(button-primary-hover-color, prop(primary));
}
:host([secondary]:not([ghost]):hover) {
  background: prop(button-secondary-hover-background, prop(on-secondary));
  border-color: prop(button-secondary-hover-border, prop(on-secondary));
  color: prop(button-secondary-hover-color, prop(secondary));
}
:host([tertiary]:not([ghost]):hover) {
  background: prop(button-secondary-hover-background, white);
  border-color: prop(button-secondary-hover-border, white);
  color: prop(button-secondary-hover-color, prop(primary));
}
:host([large]) {
  padding: prop(button-large-padding, 0.09em 2em 0 2em);
  height: prop(button-large-height, 3.6em);
  font-size: prop(button-large-font-size, 1.2em);
}
:host([large]) {
  padding: prop(button-large-padding, 0.09em 2em 0 2em);
  height: prop(button-large-height, 3.6em);
}
:host([round]) {
  font-size: prop(button-round-font-size, 1.2em);
  width: prop(button-large-height, 3.6em);
}
:host([large][round]) {
  font-size: prop(button-large-round-font-size, 1.2em);
  width: prop(button-large-height, 3.6em);
}
```