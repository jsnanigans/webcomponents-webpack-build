# Tabs
Tabs consists of five custom components:
- `loop-tabs` - main wrapper
- `loop-tabs-header` - wrapper for header items
- `loop-tabs-header-item` - header item
- `loop-tabs-content` - wrapper for content
- `loop-tabs-content-item` - content item

## Html
```html
<loop-tabs vertical>
  <loop-tabs-header>
    <loop-tabs-header-item>One</loop-tabs-header-item>
    <loop-tabs-header-item>Two longer text</loop-tabs-header-item>
  </loop-tabs-header>

  <loop-tabs-content>
    <loop-tabs-content-item> Hello world </loop-tabs-content-item>
    <loop-tabs-content-item> Wow that was awesome </loop-tabs-content-item>
  </loop-tabs-content>
</loop-tabs>
```
## Slots
- all components have a default slot

## Attributes / Config
- `vertical` - lists tabs on the left (below each other) and content on the right


## CSS Custom properties exposed:
```css
:host .wrap {
  padding: var(--loop-theme--tabs-header-item-padding, 12px 20px);
}
:host:hover .wrap {
  background: var(--loop-theme--tabs-header-item-active-background, rgba(0,0,0,.09));
}
```