import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import './loop-tabs-content'
import './loop-tabs-content-item'
import './loop-tabs-header'
import './loop-tabs-header-item'

import store from '../../lib/store/index'

import styles from './tabs.scss'
// import template from './tabs.html'

export default class LoopTabs extends PolymerElement {

  static get properties() {
    return {
      mounted: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      vertical: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    }
  }

  static get template() {
    return html`
      <style>${html([styles])}</style>
      <slot></slot>
    `
  }
  public mounted: boolean
  public vertical: boolean
  public headerContainer: LoopTabElement
  public contentContainer: LoopTabElement
  public headerItems: LoopTabElement[]
  public contentItems: LoopTabElement[]

  public children: LoopTabElement[]
  public style: CSSStyleDeclaration

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()
    this.initializeElement()
    this.contentContainer.items = this.contentItems
    this.headerContainer.items = this.headerItems
    this.addListeners()

    if (this.vertical && this.headerContainer) {
      this.headerContainer.vertical = true
    }

    afterNextRender(this, () => {
      this.mounted = true
      this.style.display = ''

      // store.watch(this.model, (data) => {
      //   this.disabled = data
      // })
    })
  }

  public showContentAtIndex(index: number = 0) {
    this.contentItems[index].active = true
  }

  private initializeElement() {
    this.headerItems = []
    this.contentItems = []

    for (const el of this.children) {
      switch (el.nodeName) {
        case 'LOOP-TABS-HEADER':
          this.headerContainer = el
          break
        case 'LOOP-TABS-CONTENT':
          this.contentContainer = el
          break
      }
    }

    if (this.headerContainer) {
      for (const el of this.headerContainer.children) {
        if (el.nodeName === 'LOOP-TABS-HEADER-ITEM') {
          this.headerItems.push(el as LoopTabElement)
        }
      }
    }

    if (this.contentContainer) {
      for (const el of this.contentContainer.children) {
        if (el.nodeName === 'LOOP-TABS-CONTENT-ITEM') {
          this.contentItems.push(el as LoopTabElement)
        }
      }
    }
  }

  private addListeners() {
    if (this.headerContainer) {
      this.headerContainer.addEventListener('change', (e: CustomEvent) => {
        this.showContentAtIndex(e.detail.index)

        if (e.detail.userIntent === true) {
          this.contentItems[e.detail.index].focus()

          for (let i = 0; i < this.contentItems.length; i++) {
            this.contentItems[i].setAttribute('tabindex', i === e.detail.index ? '0' : '-1')
          }
        }
      })
    }
    // if (this.contentContainer) {
    //   this.contentContainer.addEventListener('change', (e) => {
    //     console.log(e)
    //   })
    // }
  }
}

window.customElements.define('loop-tabs', LoopTabs)
