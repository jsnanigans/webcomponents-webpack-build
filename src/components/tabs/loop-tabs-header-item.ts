import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'

import styles from './tabs-header-item.scss'

export default class LoopTabsHeaderItem extends PolymerElement {

  static get properties() {
    return {
      active: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_activeChanged',
      },
    }
  }

  static get template() {
    return html`
      <style>${html([styles])}</style>
      <div class="wrap"><slot></slot></div>
    `
  }
  public addEventListener: any
  public dispatchEvent: any

  public active: boolean = false

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.addListeners()
    })
  }

  public setActive() {
    this.active = true
  }

  private addListeners() {
    this.addEventListener('click', () => {
      this.setActive()
    })

    this.addEventListener('keypress', (e) => {
      if (e.which === 13) {
        this.setActive()
      }
    })
  }

  private _activeChanged(to) {
    this.dispatchEvent(new CustomEvent('change', {
      detail: {
        active: !!to,
      },
    }))
  }
}

window.customElements.define('loop-tabs-header-item', LoopTabsHeaderItem)
