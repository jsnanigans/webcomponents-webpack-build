import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'

import styles from './tabs-content.scss'

export default class LoopTabsContent extends PolymerElement {
  public activeIndex: Number = -1
  public items: LoopTabElement[]
  public dispatchEvent: any

  static get properties() {
    return {
      items: {
        type: Array,
        value: [],
      },
      activeIndex: {
        type: Number,
        value: -1,
        // observer: '_activeIndexChanged'
      },
    }
  }

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.addListeners()
    })
  }

  private addListeners() {
    let i = 0
    for (const el of this.items) {
      el.tabIndex = -1

      if (el.active) {
        this.activeIndex = i
        el.tabIndex = 0
      }

      el.addEventListener('change', (data: CustomEvent) => {
        const active = data.detail.active
        if (active) {
          let xi = 0
          for (const xel of this.items) {
            if (xel !== el) {
              xel.active = false
            } else {
              this.activeIndex = xi
            }
            xi++
          }
        }
      })

      i++
    }
  }

  static get template() {
    return html`
      <style>${html([styles])}</style>
      <slot></slot>
    `
  }
}

window.customElements.define('loop-tabs-content', LoopTabsContent)
