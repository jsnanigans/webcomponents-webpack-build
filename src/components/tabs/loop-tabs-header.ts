import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'

import styles from './tabs-header.scss'

export default class LoopTabsHeader extends PolymerElement {
  public items: LoopTabElement[]
  public activeIndex: number = -1
  public dispatchEvent: any

  static get properties() {
    return {
      items: {
        type: Array,
        value: [],
      },
      activeIndex: {
        type: Number,
        value: -1,
        observer: '_activeIndexChanged',
      },
      vertical: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    }
  }

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.addListeners()
      this.setupActive()
    })
  }

  private setupActive() {
    const first = this.items.length ? this.items[0] : null
    if (first && this.activeIndex === -1) {
      first.setActive()
    }
  }

  private addListeners() {
    let i = 0
    for (const el of this.items) {
      el.tabIndex = 0

      if (el.active) {
        this.activeIndex = i
      }

      el.addEventListener('change', (data: CustomEvent) => {
        const active = data.detail.active
        if (active) {
          let xi = 0
          for (const xel of this.items) {
            if (xel !== el) {
              xel.active = false
            } else {
              this.userInteractionChange(xi, xel)
              this.activeIndex = xi
            }
            xi++
          }
        }
      })

      i++
    }
  }

  private userInteractionChange(index: number, el: HTMLElement) {
    this.dispatchEvent(new CustomEvent('change', {
      detail: {
        userIntent: true,
        index,
        el,
      },
    }))
  }

  private _activeIndexChanged(to) {
    this.dispatchEvent(new CustomEvent('change', {
      detail: {
        index: to,
      },
    }))
  }

  static get template() {
    return html`
      <style>${html([styles])}</style>
      <slot></slot>
    `
  }
}

window.customElements.define('loop-tabs-header', LoopTabsHeader)
