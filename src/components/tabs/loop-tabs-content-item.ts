import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'

import styles from './tabs-content-item.scss'

export default class LoopTabsContentItem extends PolymerElement {
  public dispatchEvent: any

  static get properties() {
    return {
      active: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_activeChanged',
      },
    }
  }

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
    })
  }

  private _activeChanged(to, from) {
    this.dispatchEvent(new CustomEvent('change', {
      detail: {
        active: !!to,
      },
    }))
  }

  static get template() {
    return html`
      <style>${html([styles])}</style>
      <slot></slot>
    `
  }
}

window.customElements.define('loop-tabs-content-item', LoopTabsContentItem)
