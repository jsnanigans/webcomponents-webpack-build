# Cookie header

## Html
```html
<loop-cookie
  layout=""
  cookie-name="loop-cookie-accepted"
>
  <div slot="text">
    <h5>Verwendung von Cookies</h5>
    <p>Lorem ipsum dolor sit amet, Cookie Einstellungen</p>
  </div>
  <div slot="buttons">
    <loop-button onclick="store.action.cookies_accept()">OK ✕</loop-button>
  </div>

  <script type="template/tracking">
    console.log('tracking script 1');
  </script>
  <script type="template/tracking">
    console.log('tracking script 2');
  </script>
  <script type="template/marketing">
    console.log('marketing script')
  </script>

</loop-cookie>

```
## Scripts
The scripts for the tracking options can be defined in the root level of `<loop-cookie>`. Once the cookie options are changed and cookies are accepted.
Script tags must have a `type` attribute that starts with `template/` and is followed by the type of tracking, for example: `template/marketing` or `template/tracking`.

When the option is changed to true or when the cookie header is initiated and cookies are accepted then the script inside will be executed.

## Slots
- `text`
- `buttons`

## Attributes / Config
- `layout` Set how the cookie header should be displayed, the options are:
  - `float` is the default - fixed on the bottom of the page
  - `top` is op top of the page and pushes everything down - this will probably be removed

- `cookie-name` - Defines the name of the cookie that is set when the user accepts. The default is `cookies_accepted`

- `invert` - Enable alternative style for the button, this syntax will probably change

## CSS Custom properties exposed:
```css
.cookie__content {
  flex-direction: var(--loop-theme--cookie-flex-direction, row);
  padding: var(--loop-theme--cookie-padding, 22px 45px);
}
.cookie__text {
  margin: var(--loop-theme--cookie-text-margin, 0 3em 0 0);
}

@media (max-width: 900px) {
  .cookie__content {
    flex-direction: var(--loop-theme--cookie-flex-direction, column);
  }
  .cookie__text {
    margin: var(--loop-theme--cookie-text-margin, 0 0 1em 0);
  }
}
```

## Store bindings
### Actions
- `set_cookies_options` - pass an object, with `{key: boolean}` to set that value on the options. If no value is passed it will use: `{marketing: true, tracking: true}`. Cookies are automatically set, updated store value `cookies_allowed`
- `set_cookies_accepted` - pass boolean, will open or close the cookie header when changed and update the cookie
- `cookies_accept` - no parameters, sets `cookies_accepted` to true in the store and accepts and runs `set_cookies_options` with default options

### Store values
- `cookies_accepted` boolean - is the cookie header accepted or not
- `cookies_allowed` object: `{key: boolean}` which cookies are turned on