import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import anime from 'animejs'
import * as Cookies from 'js-cookie'

import store from '../../lib/store/index'

import template from './cookie.html'
import styles from './cookie.scss'

export default class LoopCookie extends PolymerElement {

  static get properties() {
    return {
      open: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_openChanged',
      },
      cookieName: {
        type: String,
        value: 'cookies_accepted',
      },
      layout: {
        type: String,
        value: 'float',
      },
    }
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
  public allowedCookiesString: string = 'cookies_allowed'
  public acceptedCookiesString: string
  public acceptedCookies: boolean | string
  public allowedCookies: {} = {}
  public marketingCode: string
  public trackingCode: string
  public trackingStatus = false
  public height_: number
  public children: HTMLScriptElement[]

  public style: CSSStyleDeclaration
  public scrollHeight: number

  public open: boolean
  public layout: string
  public cookieName: string

  public ready() {
    super.ready()

    afterNextRender(this, () => {
      this.acceptedCookiesString = this.cookieName
      this.acceptedCookies = Cookies.get(this.acceptedCookiesString) || false
      const allowedCArray = (
        Cookies.get(this.allowedCookiesString) || ''
      ).split('|')
      const allowedCookieObj = {}

      for (const ok of allowedCArray) {
        if (ok) {
          allowedCookieObj[ok] = true
        }
      }

      this.allowedCookies = allowedCookieObj

      store.set('cookies_accepted', () => this.acceptedCookies)
      store.set('cookies_allowed', () => this.allowedCookies)

      if (!this.acceptedCookies) {
        this._layoutSetup()
        setTimeout(() => {
          this.open = true
        }, 200)
      } else {
        this.startTracking()
      }
    })
  }

  /**
   * Attribute observers
   */
  public _openChanged(open, was) {
    if (typeof was === 'undefined') {
      return
    }

    if (open) {
      this._layoutShow()
    } else {
      this._layoutHide()
    }
  }

  public _updateAllowTracking(set, was) {
    console.log(set, was)
  }

  /**
   * private API
   */
  public _calculateHeight() {
    this.style.opacity = '0'
    this.style.position = 'absolute'
    this.style.pointerEvents = 'none'
    this.height_ = this.scrollHeight
    this.style.opacity = ''
    this.style.position = ''
    this.style.pointerEvents = ''
  }

  /**
   * Layout animations
   */
  public _layoutSetup() {
    if (this.layout === '') {
      this.layout = 'float'
    }

    this.style.display = 'block'

    switch (this.layout) {
      case 'top':
        this.style.opacity = '0'
        this.style.position = 'absolute'
        this.style.pointerEvents = 'none'
        break
      case 'float':
        this.style.opacity = '0'
    }
  }

  public _layoutShow() {
    switch (this.layout) {
      case 'top':
        this._calculateHeight()
        anime({
          targets: this,
          height: ['0', `${this.height_ || 0}px`],
          duration: 600,
          easing: 'easeOutCubic',
          complete: () => {
            this.style.height = ''
          },
        })
        break
      case 'float':
        anime({
          targets: this,
          opacity: [0, 1],
          translateY: [10, 0],
          duration: 500,
          easing: 'easeOutCubic',
          delay: 400,
        })
    }
  }

  public _layoutHide() {
    switch (this.layout) {
      case 'top':
        anime({
          targets: this,
          height: 0,
          easing: 'easeOutCubic',
          duration: 600,
        })
        break
      case 'float':
        anime({
          targets: this,
          opacity: [1, 0],
          translateY: [0, 10],
          duration: 500,
          easing: 'easeOutCubic',
          complete: () => {
            this.style.display = 'none'
          },
        })
    }
  }

  /**
   * Public API
   */

  public setCookieOptions(options: object = { marketing: true, tracking: true }) {
    for (const key in options) {
      this.allowedCookies[key] = options[key]
    }

    store.set('cookies_allowed', () => ({ ...this.allowedCookies }))

    const allowedCookiesArray = []
    for (const key in this.allowedCookies) {
      if (this.allowedCookies[key]) {
        allowedCookiesArray.push(key)
      }
    }

    Cookies.set(this.allowedCookiesString, allowedCookiesArray.join('|'))
    this.startTracking()
  }

  public setCookiesAccepted(set: boolean) {
    if (set) {
      Cookies.set(this.acceptedCookiesString, '1')
      this.open = false
      this.startTracking()
    } else {
      Cookies.remove(this.acceptedCookiesString)
      this.open = true
    }

    this.acceptedCookies = set
    store.set('cookies_accepted', () => set)
  }

  public connectedCallback() {
    super.connectedCallback()

    store.addAction('set_cookies_options', (options: object) =>
      this.setCookieOptions(options),
    )
    store.addAction('set_cookies_accepted', (e: boolean) =>
      this.setCookiesAccepted(e),
    )
    store.addAction('cookies_accept', () => {
      this.setCookiesAccepted(true)
      this.setCookieOptions()
    })
  }

  private startTracking() {
    const trackingCodeElements: HTMLElement[] = []

    if (this.acceptedCookies) {
      for (const el of this.children) {
        if (el.tagName === 'SCRIPT' && el.type.indexOf('template') === 0) {
          const type = el.type.split('/')[1]

          if (this.allowedCookies[type]) {
            el.type = 'text/javascript'
            el.innerHTML = el.innerHTML
          }
        }
      }
    }
  }
}

window.customElements.define('loop-cookie', LoopCookie)
