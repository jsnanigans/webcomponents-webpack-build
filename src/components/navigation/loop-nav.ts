import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import anime from 'animejs'

import store from '../../lib/store/index'

import template from './nav.html'
import styles from './nav.scss'

export default class LoopNav extends PolymerElement {
  static get properties() {
    return {
      mounted: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      banner: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_bannerChanged',
      },
      switch: {
        value: false,
        reflectToAttribute: true,
        observer: '_switchChanged',
      },
      sticky: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_stickyChanged',
      },
      visible: {
        type: Boolean,
        value: '',
        reflectToAttribute: true,
        observer: '_visibleChanged',
      },
      settings: {
        type: String,
        value: '',
      },
      hasUpdated: {
        type: Function,
        value: (newHeight: number) => newHeight,
      },
    }
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
  public mounted: boolean
  public visible: boolean
  public switch: boolean | string
  public sticky: boolean
  public banner: boolean
  public settings: string

  public settingsData: {
    visible?: boolean;
    sticky?: boolean;
    banner?: boolean;
    switch?: boolean;
  } = {}

  public placeholderElement: HTMLElement
  public topElement: HTMLElement
  public innerElement: HTMLElement
  public initialShow = true
  public navHeight = 0

  public shadowRoot: HTMLElement

  public setAttribute: (attr: string, value: any) => void
  public tabIndex: number
  public after: (el: any) => void
  public clientHeight: number
  public style: CSSStyleDeclaration
  private scrollElement = window

  private placeHolderVisible = false

  private updateDebuffer = null
  public hasUpdated: (newHeight: number) => void = (e) => e

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      store.set('%%component/navigation', () => this)
      this.topElement = this.shadowRoot.querySelector('.lc_nav__top')
      this.innerElement = this.shadowRoot.querySelector('.lc_nav__inner')
      this.navHeight = this.clientHeight
      if (this.switch === '') {
        this.switch = true
      }

      this.mounted = true
      const propsSettings = {
        sticky: !!this.sticky,
        visible: !!this.visible,
        banner: !!this.banner,
        switch: !!this.switch,
      }

      if (this.settings) {
        store.set(this.settings, () => propsSettings)
        store.watch(
          this.settings,
          (data) => {
            this.applySettings(data)
          },
          {
            root: true,
          },
        )
      }

      this.addScrollListeners()

      if (this.banner) {
        this.toggleBanner(true)
      } else {
        this.initialShow = false
      }
    })
  }

  public addScrollListeners() {
    this.scrollElement.addEventListener('scroll', (e) => {
      if (this.switch) {
        this.handleScroll(true)
      }
    })
  }

  public enableSwitchState() {
    const y = this.scrollElement.scrollY

    this.switch = y > this.navHeight ? 'active' : 'inactive'
  }
  public disableSwitchState() {
    this.switch = false
  }

  public handleScroll(set: boolean = false) {
    if (set) {
      this.enableSwitchState()
    } else {
      this.disableSwitchState()
    }
    setTimeout(() => {
      this.onComponentUpdate()
    }, 200)
  }

  public applySettings(data) {
    this.visible = data.visible || false
    this.sticky = data.sticky || false
    this.banner = data.banner || false
    this.switch = data.switch || false

    this.settingsData = data
  }

  public _visibleChanged(to, from) {
    if (to !== from) {
      if (to) {
        this.show()
      } else {
        this.hide()
      }
      if (this.settings) {
        store.set(this.settings, (data) => ({ ...data, visible: to }))
      }
    }
  }

  public _switchChanged(to, from) {
    if (to !== from && this.innerElement) {
      this.navHeight = this.clientHeight
      this.handleScroll(to)
    }
  }

  public _stickyChanged(to, from) {
    if (to !== from) {
      this.toggleSticky(to)
      if (this.settings) {
        store.set(this.settings, (data) => ({ ...data, sticky: to }))
      }
      this.onComponentUpdate()
    }
  }

  public _bannerChanged(to, from) {
    if (to !== from) {
      this.toggleBanner(to)
      if (this.settings) {
        store.set(this.settings, (data) => ({ ...data, banner: to }))
      }
    }
  }

  public toggleBanner(to) {
    if (!this.topElement || !this.innerElement) {
      return
    }

    if (to) {
      // show banner
      const height = this.topElement.clientHeight

      anime({
        targets: this,
        translateY: [0, height],
        easing: 'easeOutCubic',
        duration: 400,
        delay: this.initialShow ? 500 : 0,
      })
      this.onComponentUpdate()

      this.updatePlaceholder()

      this.initialShow = false
    } else {
      const height = this.topElement.clientHeight

      anime({
        targets: this,
        translateY: [height, 0],
        easing: 'easeOutCubic',
        duration: 400,
        delay: this.initialShow ? 500 : 0,
      })
      this.onComponentUpdate()

      this.updatePlaceholder()
    }
  }

  public show() {}

  public hide() {}

  public toggleSticky(set: boolean) {
    this.placeHolderVisible = set
    if (set) {
      this.updatePlaceholder()
    } else {
      this.removePlaceholder()
    }
  }

  public updatePlaceholder() {
    if (!this.placeholderElement) {
      this.createPlaceholder()
    }

    if (!this.placeHolderVisible) {
      return
    }

    this.placeholderElement.style.display = 'block'
    this.placeholderElement.style.height = this.clientHeight + 'px'

    if (this.topElement) {
      const bannerHeight = this.topElement.clientHeight
      this.placeholderElement.style.paddingTop = this.banner ? bannerHeight + 'px' : ''
    }
  }

  public createPlaceholder() {
    this.placeholderElement = document.createElement('div')
    this.placeholderElement.style.height = this.clientHeight + 'px'
    this.placeholderElement.style.display = 'none'
    this.after(this.placeholderElement)
  }

  public removePlaceholder() {
    if (!this.placeholderElement) {
      this.createPlaceholder()
    }

    this.placeholderElement.style.display = 'none'
  }

  public closeBanner() {
    this.banner = false
  }
  private onComponentUpdate() {
    let newHeight = this.clientHeight
    if (this.banner) {
      newHeight += this.topElement.clientHeight
    }

    clearTimeout(this.updateDebuffer)
    this.updateDebuffer = setTimeout(() => {
      this.hasUpdated(newHeight)
    }, 1)
  }
}

window.customElements.define('loop-nav', LoopNav)
