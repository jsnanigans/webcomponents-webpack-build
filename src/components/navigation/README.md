# Navigation

## Html
```html
<loop-nav settings="nav" visible sticky switch>
  <div slot="top">
    <div class="bar">Some text that is above nav</div>
  </div>
  <div slot="start">
    My Logo
  </div>
  <div slot="center">
    <ul>
      <li>new</li>
      <li>best sellers</li>
      <li>eyes</li>
      <li>lips</li>
      <li>face</li>
      <li>sale</li>
    </ul>
  </div>
  <div slot="end">
    <loop-button round>🚀</loop-button>
    <loop-button rounded badge="3">Add to cart</loop-button>
  </div>
</loop-nav>
```
## Slots
- `top` content for the "banner"
- `start` content for the left hand side of the nav, for example: logo
- `center` content for the center, for example: navigation links
- `end` content for the right hand side, for example: Language select
- there is a default slot or everything where no slot is defined, this content is places inside .lc_nav__inner

### Attributes / Config
- `banner` - show the `top` slot immediately
- `sticky` - position fixed for navigation, on top
- `switch` - change style when windows is scrolled past the navigation (only works in combination with `sticky`)
- `visible` - hide or show the navigation
- `settings` - optional a reference to the store with some options for the navigation:
```javascript
{
  visible: boolean,
  sticky: boolean,
  banner: boolean,
  switch: boolean
}
```

## CSS Custom properties exposed:
```css
:host {
  background: prop(nav-background, white);
  color: prop(nav-color, black);
}
:host .lc_nav__inner {
  height: prop(nav-height, 6rem);
  padding: prop(nav-padding, 0 2em);
}
:host[switch="active"] .lc_nav__inner {
  height: prop(nav-switch-height, 5rem);
}
```