# Switch

## Html
```html
<loop-switch
  text="Allow tracking cookies"
  model="cookies_allowed.tracking"
></loop-switch>
```
## Slots
- none

## Attributes / Config
- `checked` - add if the initial state is checked
- `model` - Reference to a value in the sore that should be boolean, bound to the `checked` attribute.
- `text` - general label text
- `inactive-text` - label only when not checked, will use `text` if not defined
- `active-text` - label only when checked, will use `text` if not defined
- `disabled` - disables the switch and adds some styles
- `secondary` - sets the colors to primary
- `disable` - a reference to the store, auto-disables the switch

## CSS Custom properties exposed:
```css
$defaultHeight: 25px;
$defaultWidth: 50px;

:host {
  height: var(--loop-theme--switch-height, $defaultHeight);
}
.switch__text {
  padding: var(--loop-theme--switch-text-padding, 0 0 0 1em);
}
.switch__flip {
  width: var(--loop-theme--switch-width, $defaultWidth);
  min-width: var(--loop-theme--switch-width, $defaultWidth);
  height: var(--loop-theme--switch-height, $defaultHeight);
}
.switch__track {
  border-radius: var(--loop-theme--switch-height, $defaultHeight);
}
.switch__bullet {
  width: var(--loop-theme--switch-height, $defaultHeight);
  height: var(--loop-theme--switch-height, $defaultHeight);
}
```