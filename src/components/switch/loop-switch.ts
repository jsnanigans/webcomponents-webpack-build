import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import store from '../../lib/store/index'

import template from './switch.html'
import styles from './switch.scss'

export default class LoopSwitch extends PolymerElement {
  static get properties() {
    return {
      checked: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        notify: true,
        observer: '_checkedChanged',
      },
      model: {
        type: String,
        value: '',
      },
      disable: {
        type: String,
        value: '',
      },
      disabled: {
        reflectToAttribute: true,
        notify: true,
        observer: '_disabledChanged',
      },
      secondary: {
        reflectToAttribute: true,
      },
      textColor: {
        reflectToAttribute: true,
      },
      label: {
        reflectToAttribute: true,
        notify: true,
        observer: '_labelChanged',
      },
      inactiveText: {
        type: String,
      },
      activeText: {
        type: String,
      },
      text: {
        type: String,
      },
      initiateValue: {
        type: Boolean,
        value: false,
      },
    }
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
  public checked: boolean = false
  public model: string
  public initiateValue: boolean
  public activeText: string
  public inactiveText: string
  public text: string
  public statusText: string = ''
  public disabled: any
  public disable: string
  public setAttribute: (attr: string, value: any) => void
  public tabIndex: number
  public addEventListener: any
  private shadowRoot: HTMLElement
  private dispatchEvent: (e: Event) => void
  private flip: HTMLElement
  private changeEvent: Event
  private dispatchDebuffer: any

  public connectedCallback() {
    super.connectedCallback()
    this.updateStatusText()

    // wait for render to finish, then do the expensive stuff
    afterNextRender(this, () => {
      this.tabIndex = 0

      if (this.model) {
        store.watch(this.model, (data) => {
          this.checked = data
          this.updateStatusText()
        })
      }

      if (this.disable) {
        const val = store.getValue(this.disable)
        this.disabled = val

        store.watch(this.disable, (data) => {
          this.disabled = data
        })
      }

      if (this.initiateValue) {
        store.set(this.model, () => this.checked)
      } else {
        if (this.model) {
          this.checked = store.getValue(this.model)
        }
      }

      this.addListeners()

      this.setAttribute('role', 'switch')
    })
  }

  public addListeners() {
    // click on tricker toggle
    this.flip = this.shadowRoot.querySelector('.switch__trigger')

    this.flip.addEventListener('click', () => {
      this.switch()
    })

    this.addEventListener('keypress', (e) => {
      if (e.which === 13) {
        this.switch()
      }
    })
  }

  public switch() {
    if (this.disabled || this.disabled === '') {
      return
    }
    if (this.model) {
      store.set(this.model, (e) => {
        this.checked = !e
        return !e
      })
    } else {
      this.checked = !this.checked
      this.updateStatusText()
    }

    this.createCustomEvent({
      checked: this.checked,
    })
    this.dispatchDebuffedEvent(this.changeEvent)
  }

  public _checkedChanged(to, from) {
    if (this.changeEvent && to !== from) {
      this.dispatchDebuffedEvent(this.changeEvent)
    }
    this.setAttribute('aria-checked', !!to)

    this.createCustomEvent({
      checked: this.checked,
    })
  }
  public _disabledChanged(to) {
    // this.setAttribute('aria-readonly', !to)
  }
  public _labelChanged(to) {
    this.setAttribute('aria-label', to)
  }

  private updateStatusText() {
    this.statusText = this.checked
      ? this.activeText || this.text
      : this.inactiveText || this.text
  }

  private createCustomEvent(detail) {
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      detail,
    })
  }

  private dispatchDebuffedEvent(ev: Event) {
    clearTimeout(this.dispatchDebuffer)
    this.dispatchDebuffer = setTimeout(() => {
      this.dispatchEvent(ev)
    })
  }
}

window.customElements.define('loop-switch', LoopSwitch)
