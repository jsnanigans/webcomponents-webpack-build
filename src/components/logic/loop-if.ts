import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import store from '../../lib/store/index'
import styles from './if.scss'

export default class LoopIf extends PolymerElement {

  static get properties() {
    return {
      show: {
        type: String,
        value: '',
      },
      initialHide: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    }
  }

  static get template() {
    return html`
      <style>
        ${html([styles])}
      </style>
      <slot></slot>
    `
  }
  public visible = false
  public initialHide: boolean
  public show: string

  public shadowRoot: HTMLElement
  public dispatchEvent: (e: Event) => void
  public setAttribute: (attr: string, value?: any) => void
  public removeAttribute: (attr: string) => void
  private style: CSSStyleDeclaration

  public connectedCallback() {
    super.connectedCallback()
    afterNextRender(this, () => {
      store.watch(this.show, (data) => {
        this.setVisible(!!data)
        this.initialHide = false
      })

      if (!this.show) {
        console.error(
          '`loop-if` components need a attribute `show` with a reference to the store',
          this,
        )
      }
    })
  }

  public setVisible(set: boolean) {
    this.visible = set
    this.style.display = this.visible ? '' : 'none'
  }
}

window.customElements.define('loop-if', LoopIf)
