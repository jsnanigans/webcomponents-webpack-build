import { html, PolymerElement } from '@polymer/polymer'
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'

import anime from 'animejs'

import store from '../../lib/store/index'
import baseStyles from './dropdown.scss'
// import styles from './input-text.scss'

export default class LoopDropdown extends PolymerElement {

  static get properties() {
    return {
      value: {
        type: String,
        value: '',
        notify: true,
        // observer: '_valueChanged',
      },
      action: {
        type: String,
        value: 'hover',
      },
      nudged: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      open: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_openChanged',
      },
      name: {
        type: String,
        value: '',
      },
    }
  }

  // public _valueChanged(to, from) {
  // }

  static get template() {
    return html`
      <style>
        ${html([baseStyles])}
      </style>
      <div class="lc_dropdown">
        <div class="lc_dropdown__trigger">
          <slot name="trigger"></slot>
        </div>

        <div class="lc_dropdown__content">
          <slot></slot>
        </div>
      </div>
    `
  }
  public state = 'null'
  public value: string
  public triggerEl: HTMLElement
  public contentEl: HTMLElement
  public disabled: boolean
  public open: boolean
  public action: string
  public name: string
  public animationDuration = 400
  public minMargin = 5
  public nudged = false

  public shadowRoot: HTMLElement
  public dispatchEvent: (e: Event) => void
  public setAttribute: (attr: string, value?: any) => void
  public removeAttribute: (attr: string) => void

  public triggerTimeout

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.triggerEl = this.shadowRoot.querySelector('.lc_dropdown__trigger')
      this.contentEl = this.shadowRoot.querySelector('.lc_dropdown__content')
      // this.contentEl.tabIndex = -1
      this.addListener()
    })
  }

  public addListener() {
    const events = {
      on: [],
      off: [],
    }

    switch (this.action) {
      case 'hover':
        events.on = ['focus', 'mouseenter']
        events.off = ['blur', 'mouseleave']
        break
    }

    for (const event of events.on) {
      this.triggerEl.addEventListener(event, (e) => {
        this.openOverlay()
      })
      this.contentEl.addEventListener(event, (e) => {
        this.openOverlay()
      })
    }

    for (const event of events.off) {
      this.triggerEl.addEventListener(event, (e) => {
        this.closeOverlay()
      })
      this.contentEl.addEventListener(event, (e) => {
        this.closeOverlay()
      })
    }

    window.addEventListener('scroll', () => { this.closeOverlay(0) })
    window.addEventListener('resize', () => { this.closeOverlay(0) })
  }

  public closeOverlay(to = 100) {
    clearTimeout(this.triggerTimeout)
    this.triggerTimeout = setTimeout(() => {
      this.open = false
    }, to)
  }

  public openOverlay(to = 10) {
    clearTimeout(this.triggerTimeout)
    this.triggerTimeout = setTimeout(() => {
      this.open = true
    }, to)
  }

  public _openChanged(to, from) {
    if (to !== from && this.triggerEl) {
      if (to) {
        this.animateIn()
      } else {
        this.animateOut()
      }
    }
  }

  public getPositionForBelow(): PositionData {
    let left: number
    let top: number
    let nudgeLeft: number = 0
    const nudgeTop: number = 0

    const triggerRect = this.triggerEl.getBoundingClientRect()
    const triggerSize = {
      width: this.triggerEl.clientWidth,
      height: this.triggerEl.clientHeight,
    }
    const contentSize = {
      width: this.contentEl.clientWidth,
      height: this.contentEl.clientHeight,
    }
    const wh = window.innerHeight
    const ww = window.innerWidth

    const spaceBelow = wh - triggerRect.bottom
    const posFromBottom = spaceBelow - contentSize.height

    // calculate top pos
    top = triggerRect.bottom

    if (posFromBottom < this.minMargin) {
      top += posFromBottom - this.minMargin
      this.nudged = true
    } else {
      this.nudged = false
    }

    // calculate left pos
    left = triggerRect.left
    nudgeLeft = (triggerSize.width - contentSize.width) / 2
    nudgeLeft = nudgeLeft

    return {
      left: left + nudgeLeft,
      top: top + nudgeTop,
    }
  }

  public animateIn() {
    anime.remove(this.contentEl)
    const pos = this.getPositionForBelow()
    this.contentEl.removeAttribute('style')
    this.contentEl.style.opacity = '0'
    this.contentEl.style.pointerEvents = 'all'
    this.contentEl.style.left = pos.left + 'px'
    this.contentEl.style.top = pos.top + 'px'

    const actualContentPos = this.contentEl.getBoundingClientRect()
    const fixTop = pos.top - actualContentPos.top
    const fixLeft = pos.left - actualContentPos.left

    this.contentEl.style.left = (pos.left + fixLeft) + 'px'
    this.contentEl.style.top = (pos.top + fixTop) + 'px'

    anime({
      targets: this.contentEl,
      opacity: [0, 1],
      translateY: ['10px', 0],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
    })
    // console.log(pos)
  }

  public animateOut() {
    anime.remove(this.contentEl)
    // console.log('out')
    anime({
      targets: this.contentEl,
      opacity: [1, 0],
      translateY: [0, '10px'],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
      begin: () => {
        this.contentEl.style.pointerEvents = 'none'
      },
      complete: () => {
        this.contentEl.removeAttribute('style')
      },
    })
  }
}

window.customElements.define('loop-dropdown', LoopDropdown)
