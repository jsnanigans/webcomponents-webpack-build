import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import anime from 'animejs'

import store from '../../lib/store/index'

import template from './sidebar.html'
import styles from './sidebar.scss'

export default class LoopSidebar extends PolymerElement {
  static get properties() {
    return {
      settings: {
        type: String,
        value: '',
      },
      mounted: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      overNav: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      open: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
        observer: '_openChanged',
      },
      right: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      background: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
      bgClose: {
        type: Boolean,
        value: false,
        reflectToAttribute: true,
      },
    }
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
  public mounted: boolean
  public settings: string
  public open: boolean
  public right: boolean
  public overNav: boolean
  public background: boolean
  public bgClose: boolean
  public settingsData: SidebarOptions = {}

  public navElement: LoopNavElement

  public classList: any
  public backgroundEl: HTMLElement
  public contentEl: HTMLElement
  public contentElNav: HTMLElement
  public animationDuration = 400
  public setAttribute: (attr: string, value: any) => void
  public tabIndex: number
  public addEventListener: any
  private initialStyles: any = {}

  private shadowRoot: HTMLElement
  private style: CSSStyleDeclaration
  private offsetWidth: number
  private clientWidth: number

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      this.mounted = true
      this.navElement = store.getValue('%%component/navigation')
      this.contentEl = this.shadowRoot.querySelector('.lc_sidebar__content')
      this.contentElNav = this.shadowRoot.querySelector(
        '.lc_sidebar__content__nav',
      )
      this.backgroundEl = this.shadowRoot.querySelector(
        '.lc_sidebar__background',
      )
      this.addEvents()

      const propsSettings: SidebarOptions = {
        open: !!this.open,
      }

      if (this.settings) {
        store.set(this.settings, () => propsSettings)
        store.watch(
          this.settings,
          (data) => {
            this.applySettings(data)
          },
          {
            root: true,
          },
        )
      }

      if (this.open) {
        setTimeout(() => {
          this.animateIn()
        }, 400)
      }
    })
  }

  public _openChanged(to, from) {
    if (to !== from && this.contentEl) {
      if (to) {
        this.animateIn()
      } else {
        this.animateOut()
      }
    }
  }

  public applySettings(data) {
    this.open = data.open || false

    this.settingsData = data
  }

  public beforeShow() {
    this.contentEl.style.display = 'block'
    if (this.overNav) {
      this.contentElNav.focus()
    } else {
      this.contentEl.focus()
    }
    // store.action.disable_scrolling()
  }

  public beforeClose() {
    // store.action.enable_scrolling()
  }

  public afterClose() {
    this.contentEl.style.display = 'none'
  }

  public animateIn() {
    anime.remove(this.contentEl)
    anime.remove(this.contentElNav)
    anime.remove(this.backgroundEl)
    const initialNavHeight = this.getContentOffsetPosition()

    this.contentElNav.tabIndex = 0
    this.contentEl.tabIndex = 0

    this.contentEl.style.top = initialNavHeight + 'px'
    this.contentElNav.style.height = initialNavHeight + 'px'

    const outPos = this.right ? '100%' : '-100%'

    if (this.background) {
      anime({
        targets: this.backgroundEl,
        opacity: [0, 1],
        begin: () => {
          this.backgroundEl.style.pointerEvents = 'all'
          this.backgroundEl.style.display = 'block'
        },
      })
    }

    anime({
      targets: this.contentEl,
      translateX: [outPos, 0],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
      begin: () => {
        this.beforeShow()
      },
      complete: () => {
        const updatedNavHeight = this.getContentOffsetPosition()

        anime({
          targets: this.contentElNav,
          duration: this.animationDuration / 2,
          easing: 'easeOutCubic',
          height: [initialNavHeight, updatedNavHeight],
        })

        anime({
          targets: this.contentEl,
          duration: this.animationDuration / 2,
          easing: 'easeOutCubic',
          top: [initialNavHeight, updatedNavHeight],
        })
      },
    })
  }

  public animateOut() {
    anime.remove(this.contentEl)
    anime.remove(this.contentElNav)
    anime.remove(this.backgroundEl)
    const outPos = this.right ? '100%' : '-100%'

    this.contentElNav.tabIndex = -1
    this.contentEl.tabIndex = -1

    if (this.background) {
      anime({
        targets: this.backgroundEl,
        opacity: [1, 0],
        begin: () => {
          this.backgroundEl.style.pointerEvents = 'none'
        },
        complete: () => {
          this.backgroundEl.style.display = 'none'
        },
      })
    } else {
      this.backgroundEl.style.display = 'none'
    }

    anime({
      targets: this.contentEl,
      translateX: [0, outPos],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
      begin: () => {
        this.beforeClose()
      },
      complete: () => {
        this.afterClose()
      },
    })
  }

  private getContentOffsetPosition() {
    let offset = 0
    if (!this.overNav && this.navElement) {
      const rect = this.navElement.getBoundingClientRect()
      offset = rect.height + rect.top
    } else if (this.overNav && this.navElement) {
      offset = this.navElement.clientHeight
    }

    if (this.overNav && offset < 50) {
      offset = 50
    }

    if (offset < 0) {
      offset = 0
    }

    return offset
  }

  private addEvents() {
    if (this.navElement) {
      this.navElement.hasUpdated = this.updateSidebarHeight.bind(this)
    }

    if (this.bgClose) {
      this.backgroundEl.addEventListener('click', () => {
        this.setVisibility(false)
      })
      this.backgroundEl.style.cursor = 'pointer'
    }

    window.addEventListener('keydown', ({ which: key }) => {
      if (key === 27) {
        this.setVisibility(false)
      }
    })
  }

  private updateSidebarHeight(newHeight: number) {
    if (this.open) {
      const currentPos = this.contentEl.getBoundingClientRect().top
      anime({
        targets: this.contentElNav,
        duration: this.animationDuration,
        easing: 'easeOutCubic',
        height: [currentPos, newHeight],
      })

      anime({
        targets: this.contentEl,
        duration: this.animationDuration,
        easing: 'easeOutCubic',
        top: [currentPos, newHeight],
      })
    }
  }

  private setVisibility(set: boolean = false) {
    if (this.open === set) {
      return
    }
    if (this.settings) {
      store.set(`${this.settings}.open`, () => set)
    }

    this.open = set

    if (this.open) {
      this.classList.add('visible')
      this.animateIn()
    } else {
      this.classList.remove('visible')
      this.animateOut()
    }
  }
}

window.customElements.define('loop-sidebar', LoopSidebar)
