import { afterNextRender } from '@polymer/polymer/lib/utils/render-status'
import { html, PolymerElement } from '@polymer/polymer/polymer-element'

import anime from 'animejs'

import store from '../../lib/store/index'

import template from './overlay.html'
import styles from './overlay.scss'

export default class LoopOverlay extends PolymerElement {
  static get properties() {
    return {
      model: {
        type: String,
        value: '',
      },
      wild: {
        type: String,
        value: '',
      },
    }
  }

  static get template() {
    return html([`<style>${styles}</style> ${template}`])
  }
  public model: string
  public wild: string
  public isOpen: boolean = false
  public classList: any
  public closeEl: HTMLElement
  public backgroundEl: HTMLElement
  public contentEl: HTMLElement
  public contentSlotEl: HTMLElement
  public animationDuration = 400
  public setAttribute: (attr: string, value: any) => void
  public tabIndex: number
  public addEventListener: any

  public innerHTML: string
  public append: any
  private initialStyles: any = {}

  private shadowRoot: HTMLElement
  private style: CSSStyleDeclaration
  private offsetWidth: number
  private clientWidth: number

  public ready() {
    super.ready()
  }

  public connectedCallback() {
    super.connectedCallback()

    afterNextRender(this, () => {
      store.watch(this.model, (data) => {
        this.parseOptions({
          open: data,
        })
      })

      this.closeEl = this.shadowRoot.querySelector('.overlay__close')
      this.backgroundEl = this.shadowRoot.querySelector('.overlay')
      this.contentEl = this.shadowRoot.querySelector('.overlay__content')
      this.contentSlotEl = this.shadowRoot.querySelector('.overlay__slot')
      this.contentEl.tabIndex = 0
      this.addEvents()

      if (this.wild) {
        this.setupWild()
      }
    })
  }

  public parseOptions(options: OverlayOptions) {
    const o: OverlayOptions = {
      open: true,
      showClose: true,
      scrolling: true,
      width: '',
      height: '',
      ...options,
    }

    // set content
    if (typeof o.template === 'string') {
      this.setTemplate(o.template)
    }

    // show or hide close button
    this.closeEl.style.display = o.showClose ? 'flex' : 'none'

    // set size
    this.contentEl.style.width = o.width || ''
    this.contentEl.style.height = o.height || ''

    // set scrolling styles
    this.style.overflow = 'auto'
    this.style.overflowY = 'scroll'
    if (typeof o.height === 'string' && o.scrolling) {
      this.backgroundEl.style.minHeight = o.height
    }

    // show or hide
    requestAnimationFrame(() => {
      this.setVisibility(o.open)

      requestAnimationFrame(() => {
        // set position
        this.updatePosition()
        if (!o.scrolling) {
          this.style.overflow = ''
          this.style.overflowY = 'hidden'
        }
      })
    })
  }

  public setTemplate(tmpl: string) {
    const el = document.createElement('div')
    el.innerHTML = tmpl
    this.innerHTML = ''
    this.append(el)
    window.detectModules(this.contentSlotEl)
  }

  private setupWild() {
    store.addAction(`overlay_${this.wild}`, (options) =>
      this.parseOptions(options),
    )
  }

  private updatePosition() {
    const ww = window.innerWidth
    const wh = window.innerHeight
    const ew = this.contentEl.clientWidth
    const eh = this.contentEl.clientHeight

    let setLeft = ww / 2 - ew / 2
    let setTop = wh / 2 - eh / 2

    if (setLeft < 0) {
      setLeft = 0
    }
    if (setTop < 0) {
      setTop = 0
    }

    this.contentEl.style.left = setLeft + 'px'
    this.contentEl.style.top = setTop + 'px'
  }

  private enableInnerScrolling() {
    const nav: LoopNavElement | null = store.getValue('%%component/navigation')

    const scrollbarWidth = this.offsetWidth - this.clientWidth

    this.initialStyles.marginRight = document.body.style.marginRight
    this.initialStyles.overflow = document.body.style.overflow

    if (nav && nav.sticky) {
      nav.style.right = `${scrollbarWidth}px`
    }
    document.body.style.marginRight = `${scrollbarWidth}px`
    document.body.style.overflow = 'hidden'
  }

  private disableInnerScrolling() {
    const nav: LoopNavElement | null = store.getValue('%%component/navigation')
    if (nav && nav.sticky) {
      nav.style.right = '0'
    }
    document.body.style.marginRight = this.initialStyles.marginRight
    document.body.style.overflow = this.initialStyles.overflow
  }

  private addEvents() {
    this.closeEl.addEventListener('click', () => {
      this.setVisibility(false)
    })
    this.closeEl.addEventListener('keypress', (e) => {
      if (e.which === 13) {
        this.setVisibility(false)
      }
    })

    this.backgroundEl.addEventListener('click', (e) => {
      if (e.target === this.backgroundEl) {
        this.setVisibility(false)
      }
    })

    window.addEventListener('keydown', ({ which: key }) => {
      if (key === 27) {
        this.setVisibility(false)
      }
    })

    window.addEventListener('resize', () => {
      requestAnimationFrame(() => this.updatePosition())
    })
  }

  private afterClose() {
    this.disableInnerScrolling()
  }

  private setVisibility(set: boolean = false) {
    if (this.isOpen === set) {
      return
    }

    this.isOpen = set

    if (this.model) {
      store.set(this.model, () => this.isOpen)
    }

    if (this.isOpen) {
      this.classList.add('visible')
      this.animateIn()
    } else {
      this.classList.remove('visible')
      this.animateOut()
    }
  }

  private animateIn() {
    anime.remove(this)
    anime.remove(this.contentEl)
    anime({
      targets: this,
      opacity: [0, 1],
      // translateY: ['-20px', 0],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
      begin: () => {
        this.style.display = 'block'
        store.action.disable_scrolling()
        this.contentEl.focus()
        this.enableInnerScrolling()
      },
      complete: () => {},
    })
    anime({
      targets: this.contentEl,
      translateY: ['-20px', 0],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
    })
  }

  private animateOut() {
    anime.remove(this)
    anime.remove(this.contentEl)
    anime({
      targets: this,
      opacity: [1, 0],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
      begin: () => {
        store.action.enable_scrolling()
      },
      complete: () => {
        this.style.display = 'none'
        this.afterClose()
      },
    })
    anime({
      targets: this.contentEl,
      translateY: [0, '20px'],
      duration: this.animationDuration,
      easing: 'easeOutCubic',
    })
  }
}

window.customElements.define('loop-overlay', LoopOverlay)
