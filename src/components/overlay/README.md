# Overlay

## Html

```html
<loop-overlay model="cookie_overlay_open">
  <h1>Hello Overlay</h1>
</loop-overlay>
```

### `wild` overlays

A wild overlay is initiated without any content and is then filled from somewhere else.

```html
<loop-overlay wild="anything"></loop-overlay>
```

To open it run:

```javascript
const options = {
  template: "<h1>Hello overlay</h1>"
};
loops.action.overlay_anything(options);
```

### Options

- `open` - default `false`: opens or closes the overlay
- `showClose` - default `true`: hides or shows the `✕`
- `scrolling` - default `true`: enables scrolling inside the overlay if the content is higher than the viewport
- `width` - sets a fixed width to the content, will be limited by viewport width
- `height` - sets a fixed height, will be limited by viewport height if `scrolling: false`, if `scrolling: true` then there is no limitation
- `template` - html content for overlay

## Slots

- `default` content inside the overlay

## Attributes / Config

- `model` - Required - Reference to a value in the sore that should be boolean, determines if the overlay is open.

## CSS Custom properties exposed:

```css
:host {
  background: var(--loop-theme--overlay-background, rgba(0, 0, 0, 0.3));
}
.overlay__content {
  padding: var(--loop-theme--overlay-content-padding, 40px);
  box-shadow: var(
    --loop-theme--overlay-content-shadow,
    0 10px 40px 0 rgba(0, 0, 0, 0.2)
  );
}
.overlay__close {
  width: var(--loop-theme--overlay-close-width, 40px);
  height: var(--loop-theme--overlay-close-height, 40px);
  color: var(--loop-theme--overlay-close-color, #222);
  transition-duration: var(--loop-theme--transition-duration, 0.2s);
  transition-timing-function: var(
    --loop-theme--transition-timing-function cubic-bezier(0.215, 0.61, 0.355, 1)
  );
}
.overlay__close:hover {
  color: var(--loop-theme--overlay-close-hover-color, #888);
}
```
