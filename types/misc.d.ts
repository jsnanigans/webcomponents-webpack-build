declare module '*.html' {
  const value: string;
  export default value
}
declare module '*.scss' {
  const value: string;
  export default value
}

interface Window {
  loadModule: any
  anime: any
  detectModules: (el?: Element | Document) => void
  loops: any
}

interface OverlayOptions {
  open?: boolean
  showClose?: boolean
  scrolling?: boolean
  width?: string
  height?: string
  template?: string
}


interface PositionData {
  left: number,
  top: number
}

interface LoopValidatorParams {
  name: string // name of the input, or label, or placeholder
  value: boolean | string | number // the value of the input or if its checked
  meta?: string
}

interface LoopValidatorResponse {
  message?: string,
  valid: boolean
}

interface LoopValidators {
  [key: string]: (LoopValidatorParams) => LoopValidatorResponse
}

interface LoopRegisterValidator {
  key: string,
  handler: (LoopValidatorParams) => boolean
}
