interface LoopTabElement extends Element {
  active: boolean
  items: LoopTabElement[]
  vertical: boolean
  focus: () => void
  setActive: () => void
  tabIndex: number
}

interface LoopNavElement extends HTMLElement {
  sticky: boolean
  visible: boolean
  hasUpdated: (newHeight: number) => void
}

interface SidebarOptions {
  open?: boolean
  // showClose?: boolean
  scrolling?: boolean
  // width?: string
  // height?: string
  // template?: string
}
