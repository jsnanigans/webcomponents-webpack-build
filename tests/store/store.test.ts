import Molly from '../../src/lib/store/store'
let store

describe('Store works as expected', () => {
  beforeEach(() => {
    store = new Molly({})
  })

  test('set and get value', () => {
    store.set('color', () => 'red')
    const val = store.getValue('color')
    expect(val).toBe('red')
  })

  test('set and get value with negation', () => {
    store.set('trueValue', () => true)
    const valNegated = store.getValue('!trueValue')
    expect(valNegated).toBe(false)
  })
  test('set and get value with eq', () => {
    store.set('number', () => 500)
    const valLarger = store.getValue('200 > number')
    const valSmaller = store.getValue('200 < number')
    const valLargerEq = store.getValue('500 >= number')
    const valSmallerEq = store.getValue('500 <= number')

    // store.watch('500 > number', (val) => {
    //   console.log(val)
    // }, { lazy: true })

    store.set('number', () => 200)

    expect(valLarger).toBe(false)
    expect(valLargerEq).toBe(true)
    expect(valSmaller).toBe(true)
    expect(valSmallerEq).toBe(true)
  })

  test('deep set and get value', () => {
    store.set('deep.color', () => 'red')
    const val = store.getValue('deep.color')

    expect(val).toBe('red')
  })

  test('deep set and get value with negation', () => {
    store.set('deep.trueValue', () => true)
    const valNormal = store.getValue('deep.trueValue')
    const valNegated = store.getValue('!deep.trueValue')

    expect(valNormal).toBe(true)
    expect(valNegated).toBe(false)
  })

  test('watch for change', () => {
    let val = ''
    store.watch('watchValue', (value) => {
      val = value
    })

    store.set('watchValue', () => 'updated value')

    expect(val).toBe('updated value')
  })

  test('deep watch for change', () => {
    let val = ''
    store.watch('deep.watchValue', (value) => {
      val = value
    })

    store.set('deep.watchValue', () => 'updated value')

    expect(val).toBe('updated value')
  })

  test('watch for change with negation', () => {
    let val = ''
    store.watch('!trueValue', (value) => {
      val = value
    })

    store.set('trueValue', () => true)

    expect(val).toBe(false)
  })

  test('deep watch for change with negation', () => {
    let val = ''
    store.watch('!deep.trueValue', (value) => {
      val = value
    })

    store.set('deep.trueValue', () => true)

    expect(val).toBe(false)
  })

})
